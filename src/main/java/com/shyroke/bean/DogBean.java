package com.shyroke.bean;

public class DogBean {
	private String dogid;
	private String dogname;

	public String getDogid() {
		return dogid;
	}

	public void setDogid(String dogid) {
		this.dogid = dogid;
	}

	public String getDogname() {
		return dogname;
	}

	public void setDogname(String dogname) {
		this.dogname = dogname;
	}

}
