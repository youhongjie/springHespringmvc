package com.shyroke.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.shyroke.bean.UserBean;
import com.shyroke.service.UserSevice;

@Controller
@RequestMapping(value="userAction")
public class UserAction {

	@Autowired
	private UserSevice UserSevice;
	
	@RequestMapping(value="save")
	public String save(UserBean user){
		
		UserSevice.save(user);
		
		return "success";
	}
}
